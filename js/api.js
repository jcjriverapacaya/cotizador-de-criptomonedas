class Api {
  constructor(ApiKey){
    this.ApiKey = ApiKey

  }

  async obtenerDatos() {
    const url = `https://min-api.cryptocompare.com/data/all/coinlist?api_key=${this.ApiKey}`
    const monedas = await fetch(url)
    const moneda = await monedas.json()

    return {moneda}


  }
}